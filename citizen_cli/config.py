from pydantic import BaseConfig, Field, HttpUrl, validator


class Settings(BaseConfig):
    api_url: HttpUrl = Field(...)

    class Config:
        case_sensitive = True

    @validator("api_url")
    def validate_api_url(cls, value):
        if value[-1] != "/":
            value += "/"
        return value


settings = Settings()
