import httpx

from citizen_cli.const.urls import OBSERVATIONS_URL
from citizen_cli.observations.exceptions import GetObservationsAPIError
from citizen_cli.observations.models import Observation
from citizen_cli.utils.features import (
    extract_features_from_collection,
    extract_properties_and_geometry_from_features,
)


def get_observations(client: httpx.Client, project_id: int):
    resp = client.get(OBSERVATIONS_URL.format(project_id=project_id))

    if resp.status_code != httpx.codes.OK:
        raise GetObservationsAPIError

    observations = extract_properties_and_geometry_from_features(
        extract_features_from_collection(resp.json())
    )

    return [Observation(**obs) for obs in observations]
