from typing import List, Optional

from pydantic import BaseModel


class Geometry(BaseModel):
    type: str = "Point"
    coordinates: List[float]


class Municipality(BaseModel):
    name: str
    code: Optional[str]


class Observer(BaseModel):
    username: Optional[str]
    userAvatar: Optional[str]


class Taxref(BaseModel):
    cd_nom: int
    id_statut: Optional[str]
    id_habitat: Optional[int]
    id_rang: Optional[str]
    phylum: Optional[str]
    classe: Optional[str]
    regne: Optional[str]
    ordre: Optional[str]
    famille: Optional[str]
    sous_famille: Optional[str]
    tribu: Optional[str]
    cd_taxsup: Optional[int]
    cd_sup: Optional[int]
    cd_ref: Optional[int]
    lb_nom: Optional[str]
    lb_auteur: Optional[str]
    nom_complet: Optional[str]
    nom_complet_html: Optional[str]
    nom_vern: Optional[str]
    nom_valide: Optional[str]
    nom_vern_eng: Optional[str]
    group1_inpn: Optional[str]
    group2_inpn: Optional[str]
    url: Optional[str]


class Media(BaseModel):
    id_media: int
    cd_ref: Optional[int]
    titre: str
    url: Optional[str]
    chemin: Optional[str]
    auteur: Optional[str]
    desc_media: Optional[str]
    source: Optional[str]
    licence: Optional[str]
    is_public: bool
    supprime: bool
    id_type: int
    nom_type_media: str
    desc_type_media: str


class Observation(BaseModel):
    geometry: Geometry
    municipality: Municipality
    observer: Observer
    image: Optional[str]
    id_observation: int
    id_program: int
    cd_nom: int
    date: str
    count: Optional[int]
    comment: Optional[str]
    json_data: Optional[dict]
    obs_txt: str
    timestamp_create: str
    nom_francais: str
    taxref: Taxref
    medias: List[Media]
