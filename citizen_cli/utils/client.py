from contextlib import contextmanager
from typing import Union

import httpx


@contextmanager
def get_client(*, base_url: Union[httpx.URL, str] = ""):
    with httpx.Client(base_url=base_url) as client:
        yield client
