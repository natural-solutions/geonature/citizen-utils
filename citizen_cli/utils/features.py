from typing import Iterator


def extract_features_from_collection(feature_collection: dict) -> Iterator:
    features = feature_collection.get("features", [])
    for feature in features:
        yield feature


def extract_properties_from_features(features: Iterator) -> Iterator[dict]:
    for feature in features:
        yield feature.get("properties", {})


def extract_properties_and_geometry_from_features(
    features: Iterator,
) -> Iterator[dict]:
    for feature in features:
        yield {"geometry": feature.get("geometry", {}), **feature.get("properties", {})}
