import urllib.parse

PROGRAMS_URL = "programs"
OBSERVATIONS_URI = "{project_id}/observations"
OBSERVATIONS_URL = urllib.parse.urljoin(PROGRAMS_URL + "/", OBSERVATIONS_URI)
