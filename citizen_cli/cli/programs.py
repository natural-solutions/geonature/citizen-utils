import typer
from pydantic import HttpUrl, ValidationError, parse_obj_as
from rich.console import Console
from rich.table import Table
from typing_extensions import Annotated

from citizen_cli.programs.api import get_programs
from citizen_cli.utils.client import get_client

app = typer.Typer()
console = Console()
error_console = Console(stderr=True, style="bold red")


@app.command()
def list(url: Annotated[str, typer.Option()]):
    try:
        parse_obj_as(HttpUrl, url)
    except ValidationError as e:
        error_console.print(f"You need to provide a valid http url: {str(e)}")
        raise typer.Abort()

    with get_client(base_url=url) as client:
        programs = get_programs(client=client)

    table = Table("title", "short_desc")
    for program in programs:
        table.add_row(program.title, program.short_desc)
    console.print(table)
