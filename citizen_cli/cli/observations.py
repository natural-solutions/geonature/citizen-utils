import typer
from pydantic import HttpUrl, ValidationError, parse_obj_as
from rich.console import Console
from rich.table import Table
from typing_extensions import Annotated

from citizen_cli.observations.api import get_observations
from citizen_cli.utils.client import get_client

app = typer.Typer()
console = Console()
error_console = Console(stderr=True, style="bold red")


@app.command()
def list(
    url: Annotated[str, typer.Option()], program_id: Annotated[int, typer.Option()]
):
    try:
        parse_obj_as(HttpUrl, url)
    except ValidationError as e:
        error_console.print(f"You need to provide a valid http url: {str(e)}")
        raise typer.Abort()

    with get_client(base_url=url) as client:
        # FIXME: change project into program
        observations = get_observations(client=client, project_id=program_id)

    table = Table("Date", "Cd_nom", "Nom français", "Username")
    for observation in observations:
        table.add_row(
            observation.date,
            str(observation.cd_nom),
            observation.nom_francais,
            observation.observer.username,
        )
    console.print(table)
