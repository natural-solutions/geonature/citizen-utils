import typer

from citizen_cli.cli import observations, programs

app = typer.Typer()
app.add_typer(programs.app, name="programs")
app.add_typer(observations.app, name="observations")


def main():
    app()
