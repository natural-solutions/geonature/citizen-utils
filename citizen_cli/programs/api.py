from typing import List

import httpx

from citizen_cli.const.urls import PROGRAMS_URL
from citizen_cli.programs.exceptions import GetProgramsAPIError
from citizen_cli.programs.models import Program
from citizen_cli.utils.features import (
    extract_features_from_collection,
    extract_properties_from_features,
)


def get_programs(client: httpx.Client) -> List[Program]:
    resp = client.get(PROGRAMS_URL)
    if resp.status_code != httpx.codes.OK:
        raise GetProgramsAPIError

    programs = extract_properties_from_features(
        extract_features_from_collection(resp.json())
    )

    return [Program(**program) for program in programs]
