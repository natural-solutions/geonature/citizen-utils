import datetime
from typing import Optional

from pydantic import UUID4, BaseModel


class Program(BaseModel):
    id_program: int
    id_project: int
    title: str
    short_desc: str
    long_desc: str
    form_message: Optional[str]
    image: Optional[str]
    logo: Optional[str]
    id_module: int
    taxonomy_list: Optional[int]
    is_active: bool
    timestamp_create: datetime.datetime
    timestamp_update: Optional[datetime.datetime]


class Module(BaseModel):
    id_module: int
    name: str
    label: str
    desc: Optional[str]
    icon: Optional[str]
    on_sidebar: Optional[str]
    timestamp_create: datetime.datetime
    timestanp_update: Optional[datetime.datetime]


class Project(BaseModel):
    id_project: int
    unique_id_project: UUID4
    name: str
    short_desc: Optional[str]
    long_desc: Optional[str]
    timestamp_create: datetime.datetime
    timestamp_update: Optional[datetime.datetime]
