import pytest

from citizen_cli.cli.main import app, main


class TestMain:
    def test_app(self, runner):
        result = runner.invoke(app, ["--help"])

        assert result.exit_code == 0

    def test_app_nothing_provided(self, runner):
        result = runner.invoke(app)

        assert result.exit_code == 2
        assert "Missing command" in result.stderr

    def test_main(self):
        with pytest.raises(SystemExit):
            main()
