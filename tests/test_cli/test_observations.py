from citizen_cli.cli.main import app
from tests.mocks.api_mock import api_mock
from tests.test_utils.data import BASE_URL, PROGRAM_ID


class TestObservations:
    def test_list_help(self, runner):
        result = runner.invoke(app, ["observations", "list", "--help"])

        assert result.exit_code == 0, result.stdout

    @api_mock
    def test_list(self, runner):
        result = runner.invoke(
            app,
            ["observations", "list", "--url", BASE_URL, "--program-id", PROGRAM_ID],
        )

        assert result.exit_code == 0, result.stdout

    def test_list_wrong_url(self, runner):
        result = runner.invoke(
            app, ["observations", "list", "--url", "test", "--program-id", PROGRAM_ID]
        )

        assert result.exit_code == 1
        assert "You need to provide a valid http url" in result.stderr
