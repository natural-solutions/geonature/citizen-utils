import httpx
import pytest

from citizen_cli.programs.api import get_programs
from citizen_cli.programs.exceptions import GetProgramsAPIError
from tests.mocks.api_mock import api_mock


class TestPrograms:
    def test_get_programs(self, client):
        programs = get_programs(client=client)

        assert len(programs) != 0

    def test_get_programs_raise(self, client):
        api_mock["programs"].return_value = httpx.Response(400)

        with pytest.raises(GetProgramsAPIError):
            _ = get_programs(client=client)
