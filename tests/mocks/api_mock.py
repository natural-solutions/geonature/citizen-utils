import respx
from httpx import Response

from citizen_cli.const.urls import OBSERVATIONS_URI, PROGRAMS_URL
from tests.test_utils.data import BASE_URL, OBSERVATIONS, PROGRAMS

api_mock = respx.mock(
    base_url=BASE_URL, assert_all_mocked=False, assert_all_called=False
)
api_mock.route(
    url=PROGRAMS_URL,
    name="programs",
).mock(
    return_value=Response(200, json=PROGRAMS),
)

api_mock.route(
    path__regex=OBSERVATIONS_URI.format(project_id=r"(?P<pk>\d+)"),
    name="observations",
).mock(return_value=Response(200, json=OBSERVATIONS))
