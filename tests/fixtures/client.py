import httpx
import pytest

from tests.mocks.api_mock import api_mock
from tests.test_utils.data import BASE_URL


@pytest.fixture
def client():
    mock_transport = httpx.MockTransport(api_mock.handler)
    with httpx.Client(base_url=BASE_URL, transport=mock_transport) as client:
        yield client
