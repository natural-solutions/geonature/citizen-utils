from typing import Iterator

from citizen_cli.utils.features import (
    extract_features_from_collection,
    extract_properties_from_features,
)
from tests.test_utils.data import OBSERVATIONS


class TestFeatures:
    def test_extract_features_from_collection(self):
        features = extract_features_from_collection(OBSERVATIONS)

        assert isinstance(features, Iterator)
        one_feature = next(features)
        assert one_feature == OBSERVATIONS["features"][0]

    def test_extract_properties_from_features(self):
        features = OBSERVATIONS["features"]

        properties = extract_properties_from_features(features=features)

        assert isinstance(properties, Iterator)
        one_property = next(properties)
        assert (
            one_property == [feat["properties"] for feat in OBSERVATIONS["features"]][0]
        )

    def test_extract_features_from_collection_empty(self):
        features = extract_features_from_collection({})

        assert list(features) == []
