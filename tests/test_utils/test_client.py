import httpx

from citizen_cli.utils.client import get_client


class TestClient:
    def test_get_client(self):
        base_url = "test/"

        with get_client(base_url=base_url) as client:
            assert isinstance(client, httpx.Client)
            assert client.base_url.path == base_url

        assert client.is_closed
