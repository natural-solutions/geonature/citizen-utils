import httpx
import pytest

from citizen_cli.const.urls import OBSERVATIONS_URL
from citizen_cli.observations.api import get_observations
from citizen_cli.observations.exceptions import GetObservationsAPIError
from citizen_cli.observations.models import Observation
from tests.mocks.api_mock import api_mock


class TestApi:
    def test_observation_url(self):
        assert OBSERVATIONS_URL.format(project_id=1) == "programs/1/observations"

    def test_get_observations(self, client):
        fake_project_id = 1

        observations = get_observations(client=client, project_id=fake_project_id)

        assert all(isinstance(obj, Observation) for obj in observations)
        assert observations[0].id_observation == 2
        assert observations[0].id_program == 1

    def test_get_observations_raise(self, client):
        fake_project_id = 1

        api_mock["observations"].return_value = httpx.Response(400)

        with pytest.raises(GetObservationsAPIError):
            _ = get_observations(client=client, project_id=fake_project_id)
